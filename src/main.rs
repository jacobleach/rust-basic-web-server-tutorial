use std::io::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;
use std::str;

static HTML: &'static str = include_str!("../html/index.html");

fn main() {
    let listener = TcpListener::bind("0.0.0.0:9999").unwrap();

    for stream in listener.incoming() {
        let stream = stream.unwrap();

        handle_connection(stream);
    }
}

fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 512];
    let get = b"GET / HTTP/1.1\r\n";

    stream.read(&mut buffer).unwrap();

    println!("Request: {}", String::from_utf8_lossy(&buffer[..]));

    if buffer.starts_with(get) {
        let response = format!("HTTP/1.1 200 OK\r\n\r\n{}", HTML);
        stream.write(response.as_bytes()).unwrap();

        println!("Responding with 200!");
    } else {
        stream.write(b"HTTP/1.1 500\r\n").unwrap();

        println!("Responding with 500!");
    }

    stream.flush().unwrap();
}
